package com.example.demo.model;

import com.example.demo.enums.AccountBookEnum;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class AccountBookItem {
    private Long id;
    private LocalDate dateCreate;
    private String accountBookEnum;
    private Long money;
    private String category;
}

