package com.example.demo.model;

import com.example.demo.enums.AccountBookEnum;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountBookRequest {

    @Enumerated(value = EnumType.STRING)
    private AccountBookEnum accountBookEnum;

    private Long money;

    private String category;

    private String etcMemo;
}
