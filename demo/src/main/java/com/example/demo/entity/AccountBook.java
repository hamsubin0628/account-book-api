package com.example.demo.entity;

import com.example.demo.enums.AccountBookEnum;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class AccountBook {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private LocalDate dateCreate;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private AccountBookEnum accountBookEnum;

    @Column(nullable = false)
    private Long money;

    @Column(nullable = false, length = 30)
    private String category;

    @Column(columnDefinition = "TEXT")
    private String etcMemo;
}
