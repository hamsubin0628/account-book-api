package com.example.demo.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AccountBookEnum {
    SPENDING("지출"),
    INCOME("수입");

    private final String form;
}
