package com.example.demo.service;

import com.example.demo.entity.AccountBook;
import com.example.demo.model.AccountBookItem;
import com.example.demo.model.AccountBookRequest;
import com.example.demo.repository.AccountBookRepository;
import lombok.RequiredArgsConstructor;
import org.apache.coyote.Request;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AccountBookService {
    private final AccountBookRepository accountBookRepository;

    public void setAccountBook(AccountBookRequest request){
        AccountBook addData = new AccountBook();
        addData.setDateCreate(LocalDate.now());
        addData.setAccountBookEnum(request.getAccountBookEnum());
        addData.setMoney(request.getMoney());
        addData.setCategory(request.getCategory());
        addData.setEtcMemo(request.getEtcMemo());

        accountBookRepository.save(addData);


    }

    public List<AccountBookItem> getAccountBooks(){
        List<AccountBook> originList = accountBookRepository.findAll();

        List<AccountBookItem> result = new LinkedList<>();

        for (AccountBook accountBook : originList){
            AccountBookItem addItem = new AccountBookItem();
            addItem.setId(accountBook.getId());
            addItem.setDateCreate(accountBook.getDateCreate());
            addItem.setMoney(accountBook.getMoney());
            addItem.setCategory(accountBook.getCategory());
            addItem.setAccountBookEnum(accountBook.getAccountBookEnum().getForm());

            result.add(addItem);
        }

        return result;
    }
}
