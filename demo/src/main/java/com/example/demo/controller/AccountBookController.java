package com.example.demo.controller;

import com.example.demo.model.AccountBookItem;
import com.example.demo.model.AccountBookRequest;
import com.example.demo.service.AccountBookService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/account")
public class AccountBookController {
    private final AccountBookService accountBookService;

    @PostMapping("/new")
    public String setAccountBook(@RequestBody AccountBookRequest request){
        accountBookService.setAccountBook(request);

        return "OK";
    }

    @GetMapping("/all")
    public List<AccountBookItem> getAccountBook() {
        return accountBookService.getAccountBooks();
    }
}
